<?php
/*This file is part of motoboyz, twentynineteen child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

function motoboyz_enqueue_child_styles() {
$parent_style = 'parent-style'; 
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 
		'child-style', 
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get('Version') );
	}
add_action( 'wp_enqueue_scripts', 'motoboyz_enqueue_child_styles' );

/*Write here your own functions */
/*
* Creating a function to create our CPT
*/
 
function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Stores', 'Post Type General Name', 'autoboys' ),
        'singular_name'       => _x( 'Store', 'Post Type Singular Name', 'autoboys' ),
        'menu_name'           => __( 'Stores', 'autoboys' ),
        'parent_item_colon'   => __( 'Parent Store', 'autoboys' ),
        'all_items'           => __( 'All Stores', 'autoboys' ),
        'view_item'           => __( 'View Store', 'autoboys' ),
        'add_new_item'        => __( 'Add New Store', 'autoboys' ),
        'add_new'             => __( 'Add New', 'autoboys' ),
        'edit_item'           => __( 'Edit Store', 'autoboys' ),
        'update_item'         => __( 'Update Store', 'autoboys' ),
        'search_items'        => __( 'Search Store', 'autoboys' ),
        'not_found'           => __( 'Not Found', 'autobys' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'autoboys' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Stores', 'autoboys' ),
        'description'         => __( 'Store location and details', 'autoboys' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'provinces' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
		'show_in_rest' 		  => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'Stores', $args );
// Registering your Custom Post Category
 	function reg_cat() {
    	register_taxonomy_for_object_type('category','stores');
	}
// Registering your Custom Post Tag	
	function reg_tag() {
    	register_taxonomy_for_object_type('post_tag', 'stores');
	}
add_action('init', 'reg_tag');
add_action('init', 'reg_cat');
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type', 0 );
/* the WordPress Way */
        $post_ids = get_posts(array('post_per_page' => -1, 'cat' => 97));
    //then update each post
        foreach($post_ids as $p){
            $po = array();
            $po = get_post($p->ID,'ARRAY_A');
            $po['post_type'] = "Stores";
            wp_update_post($po);
}